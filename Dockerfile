ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jdk8:latest

ARG VERSION=3.8.5
ARG BASE_URL=https://downloads.apache.org
ARG SHA=89ab8ece99292476447ef6a6800d9842bbb60787b9b8a45c103aa61d2f205a971d8c3ddfb8b03e514455b4173602bd015e82958c0b3ddc1728a57126f773c743

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install curl && \
    mkdir -p /usr/share/apache-maven && \
    curl -sSL ${BASE_URL}/maven/maven-3/${VERSION}/binaries/apache-maven-${VERSION}-bin.tar.gz -o apache-maven-bin.tar.gz && \
    echo "${SHA}  apache-maven-bin.tar.gz" | sha512sum -c && \
    tar xzCf /usr/share/apache-maven apache-maven-bin.tar.gz --strip-components=1 && \
    ln -s /usr/share/apache-maven/bin/mvn /usr/bin && \
    rm -f apache-maven-bin.tar.gz
